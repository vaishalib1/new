package com.weatherforecast;




import java.util.Scanner;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.Assert;

import com.weatherforecast.api.WeatherForeCastAPI;

@RunWith(SpringRunner.class)
@SpringBootTest
public class WeatherforecastApplicationTests {
	
	@Autowired
	WeatherForeCastAPI  weatherForeCastAPIService;
	

	@Test
	public void contextLoads() {
		
		char ch = 'Y';
		do{
			System.out.println("Enter zip code of city");
			String zipCode=new Scanner(System.in).nextLine();
			boolean isResponseValid=weatherForeCastAPIService.getTommorowsForeCastbyZipCode(zipCode);
			Assert.isTrue(isResponseValid);
			System.out.println("Do you want to know coolest hour of tommorow's day ? Y/N");
			ch=new Scanner(System.in).nextLine().charAt(0);
		}while(ch=='Y');
	}

}
