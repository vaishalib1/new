package com.weatherforecast;
import java.util.Scanner;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import com.weatherforecast.api.WeatherForeCastAPI;

@Component
public class CommandLineAppStartupRunner implements CommandLineRunner {
	
	@Autowired
	WeatherForeCastAPI  weatherForeCastAPIService;
	
	@Override
	public void run(String... args) throws Exception {
		char ch = 'Y';
		do{
			System.out.println("Enter zip code of city");
			String zipCode=new Scanner(System.in).nextLine();
			weatherForeCastAPIService.getTommorowsForeCastbyZipCode(zipCode);
			System.out.println("Do you want to know coolest hour of tommorow's day ? Y/N");
			ch=new Scanner(System.in).nextLine().charAt(0);
		}while(ch=='Y');
		System.out.println("Weather Forecast Finished Bye....");	
	}

}
