package com.weatherforecast.model;

import java.util.List;



public class WeatherForeCastAPIResponse {
	public String success;
	public Error error;
	public List<Response> response;
	
	
	public static class Error{
		public String code;
		public String description;
		
		public String getCode() {
			return code;
		}
		public void setCode(String code) {
			this.code = code;
		}
		public String getDescription() {
			return description;
		}
		public void setDescription(String description) {
			this.description = description;
		}
		
	}
	
	
	public static class Response{
		public List<Periods> periods;

		public List<Periods> getPeriods() {
			return periods;
		}

		public void setPeriods(List<Periods> periods) {
			this.periods = periods;
		}
	}
	
	public static class Periods implements Comparable<Periods>{
		String timestamp;
		String avgTempC;
		String dateTimeISO;
		
		public String getDateTimeISO() {
			return dateTimeISO;
		}
		public void setDateTimeISO(String dateTimeISO) {
			this.dateTimeISO = dateTimeISO;
		}
		public String getTimestamp() {
			return timestamp;
		}
		public void setTimestamp(String timestamp) {
			this.timestamp = timestamp;
		}
		
		@Override
		public int compareTo(Periods o) {
			if(o.avgTempC!=null && this.avgTempC!=null){
				Double tempC1=Double.parseDouble(this.avgTempC);
				Double tempC2=Double.parseDouble(o.avgTempC);
				if(tempC1 < tempC2)
					return -1;
				else if(tempC1 > tempC2)
					return 1;
				else
					return 0;
			}
			return 0;
		}
		public String getAvgTempC() {
			return avgTempC;
		}
		public void setAvgTempC(String avgTempC) {
			this.avgTempC = avgTempC;
		}
	}

	public String getSuccess() {
		return success;
	}

	public void setSuccess(String success) {
		this.success = success;
	}

	public Error getError() {
		return error;
	}

	public void setError(Error error) {
		this.error = error;
	}

	public List<Response> getResponse() {
		return response;
	}

	public void setResponse(List<Response> response) {
		this.response = response;
	}
	
	
	

}
