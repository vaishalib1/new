package com.weatherforecast;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;


@SpringBootApplication
public class WeatherforecastApplication {
	
	 private static final Logger LOGGER= LoggerFactory.getLogger(WeatherforecastApplication.class);
	
	 public static void main(String[] args) {
		LOGGER.info("WeatherforecastApplication Started");
		SpringApplication.run(WeatherforecastApplication.class, args);
		
	}
	
	@Bean
	   public RestTemplate getRestTemplate() {
	      return new RestTemplate();
	   }
	
}
