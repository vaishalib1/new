package com.weatherforecast.api;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.google.gson.Gson;
import com.weatherforecast.WeatherforecastApplication;
import com.weatherforecast.model.WeatherForeCastAPIResponse;
import com.weatherforecast.model.WeatherForeCastAPIResponse.Periods;


@Component
public class WeatherForeCastAPIImpl implements WeatherForeCastAPI {
	
	private static final Logger LOGGER= LoggerFactory.getLogger(WeatherForeCastAPIImpl.class);
	
	@Autowired
	RestTemplate  restTemplate;
	
	@Value("${clientId}")
	String clientId;
			
	@Value("${clientSecret}")
	String clientKey;
	
	private static final String AERIES_WEATHERAPI = "https://api.aerisapi.com/forecasts/get?client_id=<CLIENT_ID>&client_secret=<CLIENT_KEY>&id=<ZIP_CODE>&from=<FROM>&to=<To>&filter=<FILTER>";

	@Override
	public boolean  getTommorowsForeCastbyZipCode(String zipId) {
		System.out.println("Retrieving Tommorow's Weather ForeCast....");
		LOGGER.info("Retrieving Tommorow's Weather ForeCast for zipCode "+zipId);
		try{
			  HttpHeaders headers = new HttpHeaders();
		      headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		      HttpEntity <String> entity = new HttpEntity<String>(headers);
		      
		      Calendar now=Calendar.getInstance();
		      now.add(Calendar.DAY_OF_YEAR, 1);
		      now.set(Calendar.HOUR_OF_DAY, 0);
		      
		      String weatherForecastAPI=AERIES_WEATHERAPI;
		      weatherForecastAPI=weatherForecastAPI.replace("<CLIENT_ID>", clientId)
		      .replace("<CLIENT_KEY>", clientKey)
		      .replace("<ZIP_CODE>", zipId)
		      .replace("<FROM>", new SimpleDateFormat("yyyy/MM/dd").format(now.getTime()))
		      .replace("<To>", "+24hours")
		      .replace("<FILTER>", "1hr");
		      
		      
		      String apiResponse=restTemplate.exchange(weatherForecastAPI, HttpMethod.GET, entity, String.class).getBody();
				if(apiResponse!=null){
					WeatherForeCastAPIResponse response= new Gson().fromJson(apiResponse, WeatherForeCastAPIResponse.class);
					if(response!=null && response.getSuccess().equals("true")){
						if(response.getResponse()!=null && response.getResponse().size() >0){
							if(response.getResponse().get(0).getPeriods()!=null && response.getResponse().get(0).getPeriods().size() >0){
								List<Periods> periodsList=response.getResponse().get(0).getPeriods();
								Collections.sort(periodsList);
								System.out.println("Coolest Hour of Day Is(ISO) "+periodsList.get(0).getDateTimeISO());
								System.out.println("Temperature Is "+periodsList.get(0).getAvgTempC());
								LOGGER.info("Weather API Response "+apiResponse);
								LOGGER.info("Coolest Hour of Day Is(ISO) "+periodsList.get(0).getDateTimeISO());
								return true;
							}
						}
						
					}
					if(response!=null && response.getError()!=null){
						LOGGER.info("Error is"+ response.getError().getCode()+"Description "+response.getError().getDescription());
						System.out.println("ErrorCode: "+response.getError().getCode());
						System.out.println("Description: "+response.getError().getDescription());
						return true;
					}
				}
		}catch(Exception exception){
			System.out.println("Error While Retrieving Tommorow's Weather ForeCast....");
			LOGGER.error("Exception"+exception.getMessage());
			return false;
		}
		return false;
	}
	
	
	 
	 
}
